## Steps to reproduce

1. Clone this repository
2. Spin up a emulated Pixel 6 with Android 13 (Tiramisu) in Android studio
3. run `npx expo run:android`
4. Observe the issue: blurred icon on the home screen and app drawer, and a very blurry icon when the app is being opened.
